﻿require.config({
    
    baseUrl: "/Scripts",
    
    paths : {
        'jquery': [
            "//code.jquery.com/jquery-1.7.1.min",
            //If the CDN is failed, then load from this location
            "jquery-1.7.1.min"
        ],
        'jqueryUI': [
            "//code.jquery.com/ui/1.8.20/jquery-ui.min",
            //If the CDN is failed, then load from this location
            "jquery-ui-1.8.20.min"
        ],
        'jqueryTimePicker': "jquery.timePicker.min",
        'crud': "crud",
        'domReady': "domReady"
    },
    shim : {
        'jqueryUI' : {
            deps: ['jquery'],
            exports: "$"
        },
        
        'jqueryTimePicker' : {
            deps: ['jquery']
        }
    }
});

require(["jqueryUI", "jqueryTimePicker"], function () {
    // Modules that do stuff on every page are instantiated here (common functionality)
    
    $(function () {

        $("input:text.date:not(#DateOfBirth)").datepicker({ dateFormat: "dd-M-yy" });

        $("input:text#DateOfBirth").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "c-30:c-0",
            dateFormat: "dd-M-yy"
        });

        $('input:text#AdmitTime').timepicker({
            'timeFormat': 'h:i A'
        });

        $('#setTimeButton').on('click', function () {
            $('input:text#AdmitTime').timepicker('setTime', new Date());
        });
        
    });
    
});

//You can use below structure if you want same main configuration
/*define(['jquery'], function ($) {

    // Some set up 
    // code here

    // Return module with methods
    return {
        bar: function() {
            $("#FirstName").val("Arif");
        }
    };


});

require(['/Scripts/requiredjs/main.js'], function (main) {
    
    //main.bar();
        
    require(['jquery'], function($) {
            
    });
});*/