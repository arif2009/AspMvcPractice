﻿define(['jquery'], function () {
        
        var create = function() {
            return "customer added";
        };
    
        var read = function(id) {
            return "this is the customer with id = " + id;
        };
    
        var update = function() {
            return "customer updated";
        };
    
        var remove = function() {
            return "customer removed";
        };

        return {
            addCustomer: create,
            getCustomer: read,
            updateCustomer: update,
            removeCustomer: remove
        };
});