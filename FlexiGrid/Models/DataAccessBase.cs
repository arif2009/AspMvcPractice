﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace FlexiGrid.Models
{
    public class DataAccessBase : IDisposable
    {
        protected const string ConnectionString = "StudentInfoDBContext";

        protected StudentInfoDBContext DbContext { get; set; }

        protected DataAccessBase()
        {
            DbContext = new StudentInfoDBContext();
        }

        public int Save()
        {
            return DbContext.SaveChanges();
        }

        public void BulkInsert(string tableName, IDictionary<string, DataColumn> columnMappings, int batchSize, DataTable dataTableToInsert)
        {

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings[ConnectionString].ConnectionString))
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                using (var bulkCopy = new SqlBulkCopy(connection))
                {
                    foreach (var keyValue in columnMappings)
                    {
                        bulkCopy.ColumnMappings.Add(keyValue.Key, keyValue.Key);
                    }

                    bulkCopy.BatchSize = batchSize;

                    bulkCopy.DestinationTableName = tableName;

                    bulkCopy.WriteToServer(dataTableToInsert);
                }
            }
        }

        public void Dispose()
        {
            DbContext.Dispose();
        }
    }
}