﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FlexiGrid.Models
{
    public class HomeDataAccess : DataAccessBase
    {
        public HomeDataAccess(){}

        public int AddNewStudent(StudentDetail studentDetail)
        {
            DbContext.StudentDetails.Add(studentDetail);

            Save();

            return studentDetail.ID;
        }

        public void RemoveStudent(int id)
        {
            StudentDetail details = this.GetStudentDetailsById(id);

            DbContext.StudentDetails.Remove(details);

            Save();
        }

        public List<StudentDetail> GetAllStudents()
        {
            return DbContext.StudentDetails.ToList();
        }

        public StudentDetail GetStudentDetailsById(int studentId)
        {
            return DbContext.StudentDetails.Find(studentId);
        }

        public IEnumerable<Department> GetAllDepartments()
        {
            return DbContext.Departments;
        }
        
        public IEnumerable<Religion> GetAllReligion()
        {
            return DbContext.Religions;
        }

        public void UpdateStudent(StudentDetail detail)
        {
            DbContext.Entry(detail).State = EntityState.Modified;

            Save();
        }

        public bool IsEmailAvailable(string email)
        {
            return DbContext.StudentDetails.Any(s => s.Email == email);
        }
    }
}