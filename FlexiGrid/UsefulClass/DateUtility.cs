﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlexiGrid.UsefulClass
{
    public enum Month
    {
        Jan = 1,
        Feb,
        Mar,
        Apr,
        May,
        Jun,
        Jul,
        Aug,
        Sep,
        Oct,
        Nov,
        Dec
    }

    public static class DateUtility
    {
        public const string DateSequenceFormat = "d-m-y";
        public const string DateFormat = "dd-MMM-yyyy";
        public const string DateTimeFormat = "dd-MMM-yyyy h:mm tt";
        public const char Separator = '-';
        public const string SeparatorMessage = "Use existing calender to put date";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="monthName">String Month Name</param>
        /// <returns>It will return int month</returns>
        public static int MonthNameToInt(string monthName)
        {
            return (int)Enum.Parse(typeof(Month), monthName, true);
        }

        public static DateTime To24HourDateTime(int year, int month, int day, string meridiem, int hour, int minute, int second = 0, int millisecond = 0)
        {
            int hour24 = hour;
            switch (meridiem.ToLower())
            {
                case "am":
                    if (hour > 12) throw new Exception("Invalid hour.");
                    if (hour == 12) hour24 = 0;
                    break;
                case "pm":
                    if (hour > 12)
                    {
                        throw new Exception("Invalid hour.");
                    }
                    else if (hour >= 1 && hour < 12)
                    {
                        hour24 = 12 + hour;
                    }
                    break;
            }
            return new DateTime(year, month, day, hour24, minute, second, millisecond);
        }

        public static DateTime To24HourDateTime(string year, string month, string day, string meridiem, string hour, string minute, string second = "0", string millisecond = "0")
        {
            return To24HourDateTime(int.Parse(year), month.All(Char.IsLetter) ? MonthNameToInt(month) : int.Parse(month),
                                    int.Parse(day), meridiem, int.Parse(hour), int.Parse(minute), int.Parse(second),
                                    int.Parse(millisecond));
        }

        public static DateTime ToDate(int year, int month, int day)
        {
            return new DateTime(year, month, day);
        }

        /// <summary>
        /// This function convert date string to date
        /// </summary>
        /// <param name="date">10/12/2015</param>
        /// <param name="sequence">d/m/y</param>
        /// <param name="separator">/</param>
        /// <returns></returns>
        public static DateTime ToDate(string date, string sequence, char separator)
        {
            string[] dateParts = date.Split(separator);

            int[] intDateParts = dateParts.Select(datePart => datePart.All(Char.IsLetter) ? MonthNameToInt(datePart) : int.Parse(datePart)).ToArray();

            char[] sequenceParts = sequence.Split(separator).Select(char.Parse).ToArray();

            Dictionary<char, int> dateDict = sequenceParts.ToDictionary(d => d, d => intDateParts[Array.IndexOf(sequenceParts, d)]);

            return new DateTime(dateDict['y'], dateDict['m'], dateDict['d']);
        }

    }
}