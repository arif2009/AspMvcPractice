﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlexiGrid.Models;

namespace FlexiGrid.Controllers
{
    public class StudentController : Controller
    {
        private StudentInfoDBContext db = new StudentInfoDBContext();

        //
        // GET: /Student/

        public ActionResult Index()
        {
            var studentdetails = db.StudentDetails.Include(s => s.Department).Include(s => s.Religion);
            return View(studentdetails.ToList());
        }

        //
        // GET: /Student/Details/5

        public ActionResult Details(int id = 0)
        {
            StudentDetail studentdetail = db.StudentDetails.Find(id);
            if (studentdetail == null)
            {
                return HttpNotFound();
            }
            return View(studentdetail);
        }

        //
        // GET: /Student/Create

        public ActionResult Create()
        {
            ViewBag.DeptID = new SelectList(db.Departments, "ID", "Name");
            ViewBag.ReligionID = new SelectList(db.Religions, "ID", "Name");
            return View();
        }

        //
        // POST: /Student/Create

        [HttpPost]
        public ActionResult Create(StudentDetail studentdetail)
        {
            if (ModelState.IsValid)
            {
                db.StudentDetails.Add(studentdetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DeptID = new SelectList(db.Departments, "ID", "Name", studentdetail.DeptID);
            ViewBag.ReligionID = new SelectList(db.Religions, "ID", "Name", studentdetail.ReligionID);
            return View(studentdetail);
        }

        //
        // GET: /Student/Edit/5

        public ActionResult Edit(int id = 0)
        {
            StudentDetail studentdetail = db.StudentDetails.Find(id);
            if (studentdetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.DeptID = new SelectList(db.Departments, "ID", "Name", studentdetail.DeptID);
            ViewBag.ReligionID = new SelectList(db.Religions, "ID", "Name", studentdetail.ReligionID);
            return View(studentdetail);
        }

        //
        // POST: /Student/Edit/5

        [HttpPost]
        public ActionResult Edit(StudentDetail studentdetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(studentdetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DeptID = new SelectList(db.Departments, "ID", "Name", studentdetail.DeptID);
            ViewBag.ReligionID = new SelectList(db.Religions, "ID", "Name", studentdetail.ReligionID);
            return View(studentdetail);
        }

        //
        // GET: /Student/Delete/5

        public ActionResult Delete(int id = 0)
        {
            StudentDetail studentdetail = db.StudentDetails.Find(id);
            if (studentdetail == null)
            {
                return HttpNotFound();
            }
            return View(studentdetail);
        }

        //
        // POST: /Student/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            StudentDetail studentdetail = db.StudentDetails.Find(id);
            db.StudentDetails.Remove(studentdetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}