﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlexiGrid.Models;
using FlexiGrid.UsefulClass;

namespace FlexiGrid.Controllers
{
    public class HomeController : Controller
    {
        private readonly HomeDataAccess _hda;
        public HomeController()
        {
            _hda = new HomeDataAccess();
        }

        public void Test(string test)
        {
            Response.Write(DateUtility.ToDate(test, "d/m/y", '/'));
        }

        //[NonAction]
        public JsonResult IsEmailAvailable(string Email)
        {
            return Json(!_hda.IsEmailAvailable(Email));
        }

        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View(_hda.GetAllStudents());
        }

        //
        // GET: /Student/Details/5

        public ActionResult Details(int id = 0)
        {
            StudentDetail studentdetail = _hda.GetStudentDetailsById(id);
            if (studentdetail == null)
            {
                return HttpNotFound();
            }
            return View(studentdetail);
        }

        //
        // GET: /Student/Create

        public ActionResult Create()
        {
            ViewBag.DeptID = new SelectList(_hda.GetAllDepartments(), "ID", "Name");
            ViewBag.ReligionID = new SelectList(_hda.GetAllReligion(), "ID", "Name");
            return View();
        }

        //
        // POST: /Student/Create
        [HttpPost]
        public ActionResult Create(StudentDetail studentDetail)
        {
            try
            {                
                string dateOfBirth = Request.Form["DateOfBirth"];

                if (dateOfBirth.Contains(DateUtility.Separator))
                {
                    studentDetail.DateOfBirth = DateUtility.ToDate(dateOfBirth, DateUtility.DateSequenceFormat,
                                                                   DateUtility.Separator);
                }
                else
                {
                    ModelState.AddModelError("DateOfBirth", DateUtility.SeparatorMessage);
                }

                string admitDate = Request.Form["AdmitDate"];
                string admitTime = Request.Form["AdmitTime"];

                studentDetail.AdmitTime = admitTime;

                if (admitDate.Contains(DateUtility.Separator))
                {
                    studentDetail.AdmitDate = ProceedAdmitedDate(admitDate, admitTime);
                }
                else
                {
                    ModelState.AddModelError("AdmitDate", DateUtility.SeparatorMessage);
                }

                if (ModelState.IsValid)
                {
                    _hda.AddNewStudent(studentDetail);

                    return RedirectToAction("Index");
                }
            }
            catch (DbEntityValidationException e)
            {
                /*foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                            ve.PropertyName,
                            eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                            ve.ErrorMessage);
                    }
                }*/
            }
            

            ViewBag.DeptID = new SelectList(_hda.GetAllDepartments(), "ID", "Name", studentDetail.DeptID);
            ViewBag.ReligionID = new SelectList(_hda.GetAllReligion(), "ID", "Name", studentDetail.ReligionID);

            return View(studentDetail);
        }

        private DateTime ProceedAdmitedDate(string admitDate, string admitTime)
        {
            string[] admitedDate = admitDate.Split(DateUtility.Separator);
            var dateDict = new Dictionary<char, string>() {{'d', admitedDate[0]}, {'m', admitedDate[1]}, {'y', admitedDate[2]}};

            string[] admitedTime = admitTime.Split(new string[] {":", " "},
                                                                    StringSplitOptions.RemoveEmptyEntries);
            var timeDict = new Dictionary<char, string>() {{'h', admitedTime[0]}, {'m', admitedTime[1]}, {'a', admitedTime[2]}};

            return DateUtility.To24HourDateTime(dateDict['y'], dateDict['m'], dateDict['d'],
                                                                   timeDict['a'], timeDict['h'], timeDict['m']);
        }

        //
        // GET: /Student/Edit/5

        public ActionResult Edit(int id = 0)
        {
            StudentDetail studentdetail = _hda.GetStudentDetailsById(id);
            if (studentdetail == null)
            {
                return HttpNotFound();
            }

            studentdetail.AdmitTime = studentdetail.AdmitDate.ToShortTimeString();
            ViewBag.DeptID = new SelectList(_hda.GetAllDepartments(), "ID", "Name", studentdetail.DeptID);
            ViewBag.ReligionID = new SelectList(_hda.GetAllReligion(), "ID", "Name", studentdetail.ReligionID);
            return View(studentdetail);
        }

        //
        // POST: /Student/Edit/5

        [HttpPost]
        public ActionResult Edit([Bind(Exclude = "StudentId, RegNo")]StudentDetail studentdetail)
        {
            StudentDetail studentDetailFromDB = _hda.GetStudentDetailsById(studentdetail.ID);

            studentDetailFromDB.FirstName = studentdetail.FirstName;
            studentDetailFromDB.LastName = studentdetail.LastName;
            studentDetailFromDB.PresentAddress = studentdetail.PresentAddress;
            studentDetailFromDB.PremanentAddress = studentdetail.PremanentAddress;
            studentDetailFromDB.ContactNo = studentdetail.ContactNo;
            studentDetailFromDB.OptionalContactNo = studentdetail.OptionalContactNo;
            studentDetailFromDB.Email = studentdetail.Email;
            studentDetailFromDB.OptionalEmail = studentdetail.OptionalEmail;
            studentDetailFromDB.NationalID = studentdetail.NationalID;
            studentDetailFromDB.DeptID = studentdetail.DeptID;
            studentDetailFromDB.ReligionID = studentdetail.ReligionID;
            studentDetailFromDB.Sex = studentdetail.Sex;
            studentDetailFromDB.DateOfBirth = studentdetail.DateOfBirth;
            studentDetailFromDB.AdmitDate = studentdetail.AdmitDate;
            studentDetailFromDB.Website = studentdetail.Website;

            if (ModelState.IsValid)
            {
                _hda.UpdateStudent(studentDetailFromDB);
                return RedirectToAction("Index");
            }
            ViewBag.DeptID = new SelectList(_hda.GetAllDepartments(), "ID", "Name", studentDetailFromDB.DeptID);
            ViewBag.ReligionID = new SelectList(_hda.GetAllReligion(), "ID", "Name", studentDetailFromDB.ReligionID);
            return View(studentDetailFromDB);
        }

        //
        // GET: /Student/Delete/5

        public ActionResult Delete(int id = 0)
        {
            StudentDetail studentdetail = _hda.GetStudentDetailsById(id);
            if (studentdetail == null)
            {
                return HttpNotFound();
            }

            studentdetail.AdmitTime = studentdetail.AdmitDate.ToShortTimeString();
            ViewBag.DeptID = new SelectList(_hda.GetAllDepartments(), "ID", "Name", studentdetail.DeptID);
            ViewBag.ReligionID = new SelectList(_hda.GetAllReligion(), "ID", "Name", studentdetail.ReligionID);
            return View(studentdetail);
        }

        //
        // POST: /Student/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            _hda.RemoveStudent(id);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _hda.Dispose();
            base.Dispose(disposing);
        }
    }
}